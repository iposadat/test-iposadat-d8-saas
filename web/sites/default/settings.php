<?php
// @codingStandardsIgnoreFile
// Database settings.
$databases['default']['default'] = [
  'database' =>  'testdrupal',
  'username' => 'admin',
  'password' => 'admin.admin',
  'host' => 'db-gb504',
  'port' => '5505',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];
// Location of the site configuration files.
$config_directories[CONFIG_SYNC_DIRECTORY] = '../config/sync';
// Salt for one-time login links, cancel links, form tokens, etc.
#$settings['hash_salt'] = getenv('HASH_SALT');
$settings['hash_salt'] = hash("sha256","hello_world");
/**
 * Load services definition file.
 */
$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';
/**
 * The default list of directories that will be ignored by Drupal's file API.
 *
 * By default ignore node_modules folders to avoid issues
 * with common frontend tools and recursive scanning of directories looking for
 * extensions.
 *
 * @see file_scan_directory()
 * @see \Drupal\Core\Extension\ExtensionDiscovery::scanDirectory()
 */
$settings['file_scan_ignore_directories'] = [
  'node_modules',
];
/**
 * Load local development override configuration, if available.
 *
 * Use settings.local.php to override variables on secondary (staging,
 * development, etc) installations of this site. Typically used to disable
 * caching, JavaScript/CSS compression, re-routing of outgoing emails, and
 * other things that should not happen on development and testing sites.
 *
 * Keep this code block at the end of this file to take full effect.
 */
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

/* This is from the user */